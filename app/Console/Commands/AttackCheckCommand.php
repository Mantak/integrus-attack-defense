<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AttackDefense\AttackCheck;
use App\AttackDefense\LogParser;

// reads only nginx log files
/*
> 20 requests per second per domain >6min by nginx log after enabling "Attack mode” - sets rate limiting to "on"
> 15 requests per second per domain for more than 10s by nginx log - set mode to "Attack mode"
< 8 requests per second per domain for more than 20s by nginx log - sets rate limiting to "off” if current rate limiting is set to “on"
attack mode will be disabled by other command or parcing other log file, which is irrelevant for now.
*/

class AttackCheckCommand extends Command
{   
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attack:check {path : path of the web log file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Identifies an attacker from web log file. It checks nginx log files only.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = $this->argument('path');
        $logFile = new LogParser($path);
        $domainStatuses = $logFile->getDomainStatuses();
        $attackCheck = new AttackCheck($domainStatuses);

        do {
            $line = $logFile->parseLine();
            $attackCheck->checkStatus($line);

        } while ($attackCheck->stillMakesSense());

        foreach ($attackCheck->getModifiedDomainsStatuses() as $domainStatus) {
            $domainStatus->save();
        }

        $this->info("Command is complete.");
    }
}

<?php

namespace App\AttackDefense;

use Illuminate\Support\Collection;

class AttackCheck
{
    protected $TIME_LIMIT = 360; // 6 min

    protected $domainStatuses;

    protected $startStamp;

    protected $doesMakeSense;

    public function __construct(Collection $domainStatuses)
    {
        $this->domainStatuses = $domainStatuses;
        $this->doesMakeSense = true;
        $this->lastStamp = 0;
    }

    public function stillMakesSense()
    {
        return $this->doesMakeSense;
    }

    public function checkStatus($line)
    {
        if ($line == null) {
            $this->doesMakeSense = false;
            return;
        }

        $time = $line->get('timestamp');
        $domain = $line->get('domain');

        if ($this->startStamp == null) {
            $this->startStamp = $time;
        }

        $timeDiff = $this->startStamp - $time;

        if (abs($timeDiff) >= $this->TIME_LIMIT) {
            $this->doesMakeSense = false;
        }

        try {

            $domainStatus = $this->domainStatuses->where('domain', $domain)->first();

            // validation
            if ($domainStatus == null) return;

            if ($this->lastStamp == $this->startStamp) {
                $domainStatus->count++;
                return;
            }

            if ($domainStatus->attack_mode == true) {
                $this->doCheck($domainStatus, 'rate_limiting');
            } else {
                $this->doCheck($domainStatus, 'attack_mode');
            }
        } finally {
            // for next check
            $this->lastStamp = $time;
        }
    }

    protected function doCheck($domainStatus, $type): void
    {
        $timesCheck = 0;
        $cType = '';

        switch ($type) {
            case 'rate_limiting':
                if ($domainStatus->rate_limiting == false) {
                    $timesCheck = 360;
                    $cType = 'ratelimit_on';
                } else {
                    $timesCheck = 20;
                    $cType = 'ratelimit_off';
                }
                break;
            case 'attack_mode':
                $timesCheck = 10;
                $cType = 'attackmode';
                break;
            default:
                return;
        }

        $this->doesItPeak($domainStatus->count, $cType) ? $domainStatus->times++ : $domainStatus->times = 0;

        if ($domainStatus->times > $timesCheck) $domainStatus->$type = true;

        $domainStatus->count = 1;
    }

    protected function doesItPeak($count, $type)
    {
        $threshold = (int)config('attackdefense.' . $type . '_threshold');

        // var_dump($threshold);

        return $type == 'ratelimit_off' ? $count > $threshold : $count < $threshold;
    }

    // return domain attack status objects
    public function getModifiedDomainsStatuses(): Collection
    {
        return $this->domainStatuses;
    }
}

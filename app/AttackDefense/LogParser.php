<?php

namespace App\AttackDefense;

use Illuminate\Support\Collection;
use App\DomainAttackStatus;

class LogParser
{
    protected $fp, $pcre, $pos;

    public function __construct(string $path)
    {
        $this->fp = fopen($path, 'r');
        $this->pos = 0;
        

        $parser = new \Kassner\LogParser\LogParser();
        // NGINX format
        $parser->setFormat('%h %l %u %t "%r" %>s %O "%{Referer}i" \"%{User-Agent}i"');
        $this->pcre = $parser->getPCRE();
    }

    // return a collection with domain attack status objects
    public function getDomainStatuses(): Collection
    {
        $domains = config('attackdefense.check_for_domains');

        return collect($domains)->map(function ($domain) {
            $record = DomainAttackStatus::where('domain', $domain)->first();
            if (!$record) {
                return DomainAttackStatus::create(compact('domain'));
            }

            return $record;
        });
    }

    public function parseLine()
    {
        $line = collect(['domain', 'timestamp']);

        if (!preg_match($this->pcre, $this->getLine(), $matches)) {
            return null;
        }

        return $line->combine([$matches['user'], strtotime($matches['time'])]);
    }

    // Backwards reading
    protected function getLine()
    {
        $currentLine = '';

        $this->pos--;

        while (-1 !== fseek($this->fp, $this->pos, SEEK_END)) {
            $char = fgetc($this->fp);

            if (PHP_EOL == $char) {
                $currentLine = trim($currentLine);
                if(!empty($currentLine)) return $currentLine;
            } else {
                $currentLine = $char . $currentLine;
            }
            $this->pos--;
        }

        return $currentLine;
    }
}

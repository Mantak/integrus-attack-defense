<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomainAttackStatus extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public $count = 1;
    public $times = 0;
}

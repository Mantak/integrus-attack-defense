## Integrus Attack Defense Task

A tool checks web access log files to identify a target as an attacker.

Did by: Mantas Samaitis

## How to setup this project?

1. Clone this to your web server
1. Setup .env by .env.example given example
1. php artisan migrate

## Basic command

`php artisan attack:check <path>`

Use your path to your nginx log file to check any available attacks.
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Attack Defense Configuration
    |--------------------------------------------------------------------------
    |
    */

    'attackmode_threshold'      => env('ATTACKMODE_THRESHOLD', 15),
    'ratelimit_on_threshold'    => env('RATELIMIT_ON_THRESHOLD', 20),
    'ratelimit_off_threshold'   => env('RATELIMIT_OFF_THRESHOLD', 8),

    'check_for_domains'        => ['ecu.de', 'ecu.eu', 'ecu.co.uk', 'ecu.fr', 'ecu.hu']
];
